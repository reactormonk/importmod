# Importmod
![pipeline](https://gitlab.com/portmod/importmod/badges/master/pipeline.svg)
![coverage](https://gitlab.com/portmod/importmod/badges/master/coverage.svg)

A cli tool to import mods into the Portmod Repository Format.
